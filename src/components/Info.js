import { Component } from "react";

class Info extends Component{
    render() {
        let {firstname, lastName, favNumber} = this.props;
        console.log(this.props)

        return(
            <div>
                <p>My name is {lastName} {firstname} and my favourite number is {favNumber}.</p>
            </div>
        )
    }
}
export default Info;